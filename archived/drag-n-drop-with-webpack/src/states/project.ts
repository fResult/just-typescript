import { Project, ProjectStatus } from '../models/project'

type Listener<T> = (items: T[]) => void

class State<T> {
  protected listeners: Listener<T>[] = []

  addListener(listenerFn: Listener<T>) {
    this.listeners.push(listenerFn)
  }
}

export class ProjectState extends State<Project> {
  private projects: Project[] = []
  private static instance: ProjectState

  private constructor() {
    super()
  }

  static getInstance() {
    if (this.instance) {
      return this.instance
    }

    this.instance = new ProjectState()
    return this.instance
  }

  private updateListeners() {
    for (const listenerFn of this.listeners) {
      listenerFn(this.projects.slice())
    }
  }

  addProject(title: string, description: string, numOfPeople: number) {
    console.log('title', title)
    const newProject: Project = new Project(
      Date.now().toString(),
      title,
      description,
      numOfPeople,
      ProjectStatus.ACTIVE
    )

    this.projects.push(newProject)
    this.updateListeners()
  }

  moveProject(projectId: string, newStatus: ProjectStatus) {
    const foundProject = this.projects.find((p) => p.id === projectId)
    if (foundProject && foundProject.status !== newStatus) {
      foundProject.status = newStatus
      this.updateListeners()
    }
  }
}

export const projectState = ProjectState.getInstance()
