import Component from './base-component'
import { Draggable } from '../models/drag-drop'
import { Project } from '../models/project'
import { AutoBind } from '../decorators/autobind'

export class ProjectItem extends Component<HTMLUListElement, HTMLLIElement>
  implements Draggable {
  private readonly project: Project

  constructor(hostId: string, project: Project) {
    super('single-project', hostId, true, project.id)
    this.project = project
    this.configure()
    this.renderContent()
  }

  @AutoBind
  handleDragStart(event: DragEvent) {
    event.dataTransfer!.setData('text/plain', this.project.id)
    event.dataTransfer!.effectAllowed = 'move'
  }

  @AutoBind
  handleDragEnd(_: DragEvent) {
    console.log('DragEnd')
  }

  get persons() {
    if (this.project.people === 1) {
      return '1 person'
    } else {
      const project = this.project
      return `${project.people as number} persons`
    }
  }

  configure() {
    this.elem.addEventListener('dragstart', this.handleDragStart)
    this.elem.addEventListener('dragend', this.handleDragEnd)
  }

  renderContent() {
    const { title, description } = this.project
    this.elem.querySelector('h2')!.textContent = title
    this.elem.querySelector('h3')!.textContent = `${this.persons} assigned.`
    this.elem.querySelector('p')!.textContent = description
  }
}
