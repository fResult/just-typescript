import Component from './base-component'
import * as Validation from '../utils/validation'
import { AutoBind } from '../decorators/autobind'
import { projectState } from '../states/project'

export class ProjectInput extends Component<HTMLDivElement, HTMLFontElement> {
  titleInputElem: HTMLInputElement
  descInputElem: HTMLInputElement
  peopleInputElem: HTMLInputElement

  constructor() {
    super('project-input', 'app', true, 'user-input')
    this.titleInputElem = <HTMLInputElement>this.elem.querySelector('#title')!
    this.descInputElem = <HTMLInputElement>(
      this.elem.querySelector('#description')!
    )
    this.peopleInputElem = <HTMLInputElement>this.elem.querySelector('#people')!

    this.configure()
  }

  configure(): void {
    // this.formElem.addEventListener('submit', (e) => this.handleSubmit(e))
    this.elem.addEventListener('submit', this.handleSubmit)
  }

  renderContent(): void {}

  private gatherUserInput(): [string, string, number] | void {
    const enteredTitle = this.titleInputElem.value
    const enteredDescription = this.descInputElem.value
    const enteredPeople = this.peopleInputElem.value

    const titleValidatable: Validation.Validatable = {
      value: enteredTitle,
      required: true
    }
    const descValidatable: Validation.Validatable = {
      value: enteredDescription,
      required: true,
      minLength: 5
    }
    const peopleValidatable: Validation.Validatable = {
      value: +enteredPeople,
      required: true,
      min: 1,
      max: 5
    }

    if (
      !Validation.validate(titleValidatable) ||
      !Validation.validate(descValidatable) ||
      !Validation.validate(peopleValidatable)
    )
      alert('Invalid input, please try again.')
    else return [enteredTitle, enteredDescription, +enteredPeople]
  }

  private clearInputs(): void {
    this.titleInputElem.value = ''
    this.descInputElem.value = ''
    this.peopleInputElem.value = ''
  }

  @AutoBind
  private handleSubmit(e: Event) {
    e.preventDefault()
    const userInput = this.gatherUserInput()
    if (Array.isArray(userInput)) {
      const [title, desc, people] = userInput
      projectState.addProject(title, desc, people)
      this.clearInputs()
    }
  }
}
