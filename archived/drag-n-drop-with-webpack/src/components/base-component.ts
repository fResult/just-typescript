abstract class Component<T extends HTMLElement, U extends HTMLElement> {
  templateElem: HTMLTemplateElement
  hostElem: T
  elem: U

  protected constructor(
    templateId: string,
    hostElemId: string,
    isInsertAtStart: boolean,
    newElemId?: string
  ) {
    // this.templateElem = <HTMLTemplateElement>document.querySelector(`#${templateId}`)!
    this.templateElem = <HTMLTemplateElement>(
      document.getElementById(templateId)!
    )
    this.hostElem = <T>document.querySelector(`#${hostElemId}`)
    const importedNode = document.importNode(this.templateElem.content, true)
    this.elem = <U>importedNode.firstElementChild
    this.elem.id = <string>newElemId

    this.attach(isInsertAtStart)
  }

  private attach(isInsertAtStart: boolean) {
    // this.hostElem.insertAdjacentElement(isInsertAtStart ? 'beforeend' : 'afterbegin', this.formElem)
    this.hostElem.insertAdjacentElement(
      isInsertAtStart ? 'beforeend' : 'afterbegin',
      this.elem
    )
  }

  abstract configure(): void

  abstract renderContent(): void
}

export default Component
