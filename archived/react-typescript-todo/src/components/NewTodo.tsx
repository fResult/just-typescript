import React, { FormEvent, useRef } from 'react'
import '../../styles/NewTodo.css'

type NewTodoProps = {
  onAddTodo: (text: string) => void
}

const NewTodo:React.FC<NewTodoProps> = ({ onAddTodo }) => {
  const todoInputRef = useRef<HTMLInputElement>(null)

  const handleSubmitTodo = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const enterTodoText = todoInputRef.current!.value
    onAddTodo(enterTodoText)
  }
  return (
    <form onSubmit={handleSubmitTodo}>
      <div className="form-control">
        <label htmlFor="todo-text">Todo</label>
        <input id="todo-text" type="text" ref={todoInputRef} />
      </div>
      <button type="submit">Add Todo</button>
    </form>
  )
}

export default NewTodo
