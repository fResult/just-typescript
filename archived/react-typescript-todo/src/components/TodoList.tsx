import React from 'react'
import { Todo } from '../models/todo.model'
import '../../styles/TodoList.css'

interface TodoListProps {
  items: Array<Todo>
  onDeleteTodo: (id: string) => void
}

const TodoList: React.FC<TodoListProps> = (props) => {
  return (
    <div>
      <ul>
        {props.items.map((todo) => (
          <li key={todo.id}>
            <span>{todo.text}</span>
            {/* onClick={() => props.onDeleteTodo(todo.id)} */}
            <button onClick={props.onDeleteTodo.bind(null, todo.id)}>
              Delete
            </button>
          </li>
        ))}
      </ul>
    </div>
  )
}

export default TodoList
