import React, { useState } from 'react'
import './App.css'
import TodoList from './components/TodoList'
import NewTodo from './components/NewTodo'
import { Todo } from './models/todo.model'

// function App(): ComponentElement<any, any> {
const App: React.FC = () => {
  const [todos, setTodos] = useState<Array<Todo>>([])

  const handleAddTodo = (text: string) => {
    setTodos([...todos, { id: Date.now().toString(), text }])
  }
  const handleDeleteTodo = (id: string) => {
    setTodos((prevTodos) => {
      return prevTodos.filter((todo) => todo.id !== id)
    })
  }
  return (
    <div className="App">
      <NewTodo onAddTodo={handleAddTodo} />
      <TodoList items={todos} onDeleteTodo={handleDeleteTodo} />
    </div>
  )
}

export default App
