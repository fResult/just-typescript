import 'reflect-metadata'
import { plainToClass } from 'class-transformer'
import { validate, ValidationError } from 'class-validator'

import { Product } from './product.model'

const products = [
  { title: 'Carpet', price: 29.99 },
  { title: 'Book', price: 10.99 }
]

const loadedProjects = plainToClass(Product, products)

for (const project of loadedProjects) {
  console.log(project)
}

const newProd = new Product('', -9.99)
validate(newProd).then((errors: ValidationError[]) => {
  if (errors.length > 0) {
    console.error('VALIDATE ERRORS!', errors)
  } else {
    console.log(newProd.getInformation())
  }
})
