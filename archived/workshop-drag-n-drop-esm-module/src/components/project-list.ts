import Component from './base-component.js'
import { DragTarget } from '../models/drag-drop.js'
import { Project, ProjectStatus } from '../models/project.js'
import { projectState } from '../states/project.js'
import { AutoBind } from '../decorators/autobind.js'
import { ProjectItem } from './project-item.js'

export class ProjectList extends Component<HTMLDivElement, HTMLElement>
  implements DragTarget {
  assignedProjects: Project[] = []

  constructor(private type: 'active' | 'finished') {
    super('project-list', 'app', false, `${type}-projects`)

    this.configure()
    this.renderContent()
  }

  @AutoBind
  handleDragOver(event: DragEvent): void {
    if (event.dataTransfer && event.dataTransfer.types[0] === 'text/plain') {
      event.preventDefault()
      const listElem = this.elem.querySelector('ul')!
      listElem.classList.add('droppable')
    }
  }

  @AutoBind
  handleDrop(event: DragEvent): void {
    const projectId = event.dataTransfer!.getData('text/plain')
    projectState.moveProject(
      projectId,
      this.type === 'active' ? ProjectStatus.ACTIVE : ProjectStatus.FINISHED
    )

    const listElem = this.elem.querySelector('ul')!
    listElem.classList.remove('droppable')
  }

  @AutoBind
  handleDragLeave(_: DragEvent): void {
    const listElem = this.elem.querySelector('ul')!
    listElem.classList.remove('droppable')
  }

  configure(): void {
    this.elem.addEventListener('dragover', this.handleDragOver)
    this.elem.addEventListener('drop', this.handleDrop)
    this.elem.addEventListener('dragleave', this.handleDragLeave)
    projectState.addListener((projects: Project[]) => {
      const relevantProjects = projects.filter((p) => {
        if (this.type === 'active') {
          return p.status === ProjectStatus.ACTIVE
        }
        return p.status === ProjectStatus.FINISHED
      })
      this.assignedProjects = <Project[]>relevantProjects
      this.renderProjects()
    })
  }

  renderContent() {
    const listId = `${this.type}-project-list`
    this.elem.querySelector('ul')!.id = listId
    this.elem.querySelector('h2')!.textContent = `${(
      this.type || ''
    ).toUpperCase()} PROJECTS`
  }

  private renderProjects() {
    const projectsElem = document.querySelector(`#${this.type}-project-list`)!
    projectsElem.innerHTML = ''
    for (const project of this.assignedProjects) {
      new ProjectItem(this.elem.querySelector('ul')!.id, project)
    }
  }
}
