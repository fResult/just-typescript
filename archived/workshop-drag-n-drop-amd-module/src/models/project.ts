namespace App {
  export enum ProjectStatus {
    ACTIVE,
    FINISHED
  }

  export interface IProject {
    id: string
    title: string
    description: string
    people: number
  }

  export class Project implements IProject {
    constructor(
      public id: string,
      public title: string,
      public description: string,
      public people: number,
      public status: ProjectStatus
    ) {}
  }
}
