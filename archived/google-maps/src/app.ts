import { Map, View } from 'ol'
import TileLayer from 'ol/layer/Tile'
import OSM from 'ol/source/OSM'

declare let Geocoder: any

const MY_API_KEY =
  'AkZtR1-XVssaBTZ4l5RVz5n4pSMriH_ahNXikwy2gsS4pb7MGu1WmTjzOSRgIuZY'

const map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      source: new OSM()
    })
  ],
  view: new View({
    center: [0, 0],
    zoom: 0
  })
})
const geocoder = new Geocoder('nominatim', {
  provider: 'bing',
  key: MY_API_KEY,
  lang: 'en-US',
  placeholder: 'Search for your place',
  targetType: 'text-input',
  limit: 7,
  keepOpen: true
})
geocoder.on('addresschosen', function(evt: any) {
  console.log('test address chosen')
  console.info(evt)
  const feature = evt.feature
  const coord = evt.coordinate
  const address = evt.address
  const descElem = document.querySelector('.desc-map')!
  descElem.innerHTML = '<p>' + address.formatted + '</p>'
  // overlay.setPosition(coord);
  console.log({ feature, coord })
})
map.addControl(geocoder)
