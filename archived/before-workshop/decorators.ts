// Class Decorator
function Logger(loggingText: string) {
  console.log('LOGGER FACTORY')
  return function(constructor: Function) {
    console.log(loggingText)
    console.log(constructor)
  }
}

function WithTemplate(template: string, hookId: string) {
  console.log('TEMPLATE FACTORY')
  return function<T extends { new (...args: any[]): { name: string } }>(
    originalConstructor: T
  ) {
    return class extends originalConstructor {
      constructor(..._: any[]) {
        super()
        console.log('Rendering template...')
        const hookElem = document.querySelector(`#${hookId}`)!
        hookElem.innerHTML = template
        hookElem.querySelector('h1')!.textContent += ` - ${this.name}`
      }
    }
  }
}

// It will run bottom to top decorators order invoking
@Logger('LOGGING - PERSON')
@WithTemplate('<h1 style="color: aliceblue">HELLO, WORLD</h1>', 'app')
class Person {
  name = 'Korn'

  constructor() {
    console.log('Creating person object...')
  }
}

const person = new Person()
console.log(person)

/* ================================================ */

// Property Decorator
function Log(target: any, propertyName: string | Symbol) {
  console.log('Property Decorator!')
  console.log(target, propertyName)
}

// Accessor Decorator - Property
function Log2(target: any, name: string, descriptor: PropertyDescriptor) {
  console.log('Accessor decorator')
  console.log({ target, name, descriptor })
}

// Accessor Decorator - Method
function Log3(
  target: any,
  name: string | Symbol,
  descriptor: PropertyDescriptor
) {
  console.log('Method decorator')
  console.log({ target, name, descriptor })
}

// Parameter Decorator
function Log4(target: any, name: string | Symbol, position: number) {
  console.log('Parameter decorator')
  console.log({ target, name, position })
}

class Product {
  @Log public name: string
  @Log private _price: number

  constructor(name: string, price: number) {
    this.name = name
    this._price = price
  }

  @Log2
  set price(price: number) {
    if (price > 0) this._price = price
    else throw new Error('Invalid type - should be positive!')
  }

  @Log3
  getPriceWithTax(@Log4 tax: number) {
    return this.price * (1 + tax)
  }
}

const product1 = new Product('Smartphone', 15000)
const product2 = new Product('Book', 455)

function AutoBind(
  target: any,
  methodName: string,
  descriptor: PropertyDescriptor
) {
  const originalMethod = descriptor.value
  const adjustedDescriptor: PropertyDescriptor = {
    configurable: true,
    enumerable: false,
    get(): any {
      // boundFunction
      return originalMethod.bind(this)
    }
  }
  return adjustedDescriptor
}

class Printer {
  message = 'This works!'

  @AutoBind
  showMessage() {
    console.log(this.message)
  }
}

const printer = new Printer()
const button = document.querySelector('button')!
button.addEventListener('click', printer.showMessage)

/* ================================================ */

// Validate with Decorator
interface ValidatorConfig {
  [property: string]: {
    [validatableProps: string]: string[] // ['required', 'positive']
  }
}

const registeredValidators: ValidatorConfig = {}

function addValidator(className: string, propName: string, validator: string) {
  const validators =
    (registeredValidators[className]
      ? registeredValidators[className][propName]
      : [validator]) || []

  registeredValidators[className] = {
    ...registeredValidators[className],
    [propName]: [...validators, validator]
  }
}

function Required(target: any, propName: string) {
  console.log(target.constructor.name, 'construc')
  addValidator(target.constructor.name, propName, 'required')
}

function PositiveNumber(target: any, propName: string) {
  addValidator(target.constructor.name, propName, 'positive')
}

function Included(target: any, propName: string) {
  addValidator(target.constructor.name, propName, 'included')
}

function validate(obj: any) {
  const objValidatorConfig = registeredValidators[obj.constructor.name]
  let isValid = true
  if (!objValidatorConfig) return true

  for (const prop in objValidatorConfig) {
    console.log('included a', /^\w*a+\w*$/i.test(obj[prop]), prop, obj[prop])
    console.log('required', !!obj[prop], prop, obj[prop])
    console.log('positive', obj[prop] > 0, prop, obj[prop])

    for (const validator of objValidatorConfig[prop]) {
      switch (validator) {
        case 'required':
          console.log('__REQUIRED', !!obj[prop])
          isValid = isValid && !!obj[prop]
          break
        case 'positive':
          console.log('__POSITIVE', obj[prop] > 0)
          isValid = isValid && obj[prop] > 0
          break
        case 'included':
          console.log('__INCLUDED', /^\w*a+\w*$/i.test(obj[prop]))
          isValid = isValid && /^\w*a+\w*$/i.test(obj[prop])
          break
      }
    }
  }
  return isValid
}

class Course {
  @Included
  @Required
  title: string

  @PositiveNumber
  price: number

  constructor(title: string, price: number) {
    this.title = title
    this.price = price
  }
}

const courseForm = document.querySelector('form')!
courseForm.addEventListener('submit', (event) => {
  event.preventDefault()
  const title = (<HTMLInputElement>courseForm.querySelector('#title')).value
  const price = (<HTMLInputElement>courseForm.querySelector('#price')).value

  const createdCourse = new Course(title, +price)
  if (!validate(createdCourse)) {
    alert('Invalid input, please try again')
    return
  }
  console.log(createdCourse)
})
