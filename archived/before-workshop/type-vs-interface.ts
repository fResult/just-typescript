import get = Reflect.get

type PointType = {
  x: number
  y: number
}

interface PointInterface {
  x: number
  y: number
}

const getRectangleAreaType = (args: PointType) => args.x + args.y
const getRectangleAreaInterface = (args: PointInterface) => args.x + args.y

getRectangleAreaType({ x: 12, y: 4 })
getRectangleAreaInterface({ x: 12, y: 4 })

interface ThreeDimension extends PointType {
  z: number
}

interface Shape {
  area(): number
}

interface Perimeter {
  perimeter(): number
}

class Rectangle implements PointType, Shape, Perimeter {
  x: number = 2
  y: number = 4

  area(): number {
    return this.x * this.y
  }

  perimeter(): number {
    return 2 * (this.x + this.y)
  }
}

class RectanglePrism implements ThreeDimension {
  x: number = 2
  y: number = 3
  z: number = 4
}

type RectangleShape = PointType & Perimeter & Shape

class Rect implements RectangleShape {
  constructor(public x: number, public y: number) {}

  area(): number {
    return this.x * this.y
  }

  perimeter(): number {
    return 2 * (this.x + this.y)
  }
}

type PartialRectShape = PointType & Partial<Shape & Perimeter>

class PartialRect implements PartialRectShape {
  x: number = 2
  y: number = 3
}

// Weak type
const rectangle: RectangleShape = {
  x: 2,
  y: 3,
  area: () => rectangle.x * rectangle.y,
  perimeter() {
    return 2 * (this.x + this.y)
  }
}

console.log('area', rectangle.area())
console.log('perimeter', rectangle.perimeter())

type RectShapeTest = (Shape | Perimeter) & PointType

const rectTest: RectShapeTest = {
  x: 0,
  y: 0,
  area(): number {
    return 0
  },
  perimeter(): number {
    return 0
  }
}
