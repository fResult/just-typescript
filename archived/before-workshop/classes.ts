abstract class Department {
  static fiscalYear = 2021
  private employees: string[] = []
  // It couldn't assign any value for `id` because it has `readonly` keyword
  protected constructor(protected readonly id: string, protected name: string) {
    console.log(Department.fiscalYear)
  }

  static createEmployee(name: string) {
    return { name }
  }

  addEmployee(employee: string): void {
    this.employees = [...this.employees, employee]
  }

  abstract describe(this: Department): void

  printEmployeeInformation() {
    console.log(this.employees.length)
    console.log(this.employees)
  }
}

class ITDepartment extends Department {
  constructor(id: string, public admins: string[]) {
    super(id, 'IT')
  }

  printAdminInformation() {
    console.log(this.admins.length)
    console.log(this.admins)
  }

  describe(): void {
    console.log('Department:', this.id, this.name, ',', Department.fiscalYear)
  }
}

class AccountingDepartment extends Department {
  private _lastReport: string

  constructor(id: string, private reports: string[]) {
    super(id, 'Accounting')
    this._lastReport = reports[0]
  }

  get lastReport() {
    return this._lastReport
  }

  set lastReport(report: string) {
    if (!report) return
    this.addReport(report)
  }

  addReport(report: string) {
    this.reports = [...this.reports, report]
    this._lastReport = report
  }

  printReports() {
    console.log(this.reports)
  }

  describe() {
    console.log('Department ID -', this.id, this.name)
  }
}

const employee1 = Department.createEmployee('Korn')
console.log('employee1:', employee1, 'fiscalYear:', Department.fiscalYear)

const accounting = new AccountingDepartment('d1', [])
accounting.describe()
accounting.addEmployee('Buzu')
accounting.addEmployee('Bujo')
accounting.addEmployee('Buzo')
accounting.lastReport = 'Year End report'
console.log(accounting.lastReport)
accounting.addReport('Something went wrong ...!')
accounting.printEmployeeInformation()
accounting.printReports()
accounting.describe()

const it = new ITDepartment('d2', ['Korn'])
it.describe()
it.addEmployee('KornZilla')
it.addEmployee('fResult')
it.printEmployeeInformation()
it.printAdminInformation()
it.describe()

// const accCopy = { name: accounting.name, describe: accounting.describe }
// accCopy.describe()
