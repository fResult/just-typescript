// type AddFn = (a: number, b: number) => number
interface AddFn {
  (a: number, b: number): number
}

const add: AddFn = (num1: number, num2: number) => {
  return num2 + num1
}

console.log('Added result:', add(5, 3))

type NamedHistory = {
  oldName: string
  currentName: string
}

interface Named {
  name?: string

  changeName?(n: string): NamedHistory // Optional method
}

interface Greetable extends Named {
  greet(phrase: string): void
}

class Person implements Greetable {
  constructor(public name?: string) {
    if (name) this.name = name
  }

  greet(phrase: string): void {
    if (this.name) console.log(`${phrase}, I'm ${this.name}.`)
    else console.log(phrase)
  }
}

class Dog implements Greetable {
  constructor(public readonly name: string) {}

  changeName(newName: string): NamedHistory {
    return { oldName: this.name, currentName: newName }
  }

  greet(phrase: string) {
    console.log(`${phrase}, I'm ${this.name}`)
  }
}

const person1: Greetable & Named = {
  name: 'Korn',
  greet(phrase: string) {
    console.log(`${phrase} I'm ${this.name}`)
  }
}
const person2: Greetable = new Person()
const korn: Person = <Person>person2
const person3: Greetable = new Person('Kite')
person3.greet('Hello')

korn.greet('Hi')
person2.greet(`What's up`)

const dog1: Dog = new Dog('Viola')
dog1.greet('Bok Bok')
console.log(
  `The dog's name has changed: ${JSON.stringify(dog1.changeName('Pocky'))}`
)
