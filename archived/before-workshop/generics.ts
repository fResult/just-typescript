const names: Array<string | number> = [1, 2, 'Three']
if (typeof names[2] === 'string') {
  console.log((names[2] as string).split(''))
}

const promise: Promise<string> = new Promise(function(resolve, reject) {
  setTimeout(function() {
    resolve('This is done from Promise')
  }, 2000)
})

promise.then((res) => console.log(res.split(' ')))

type Person = {
  name: string
  age: number
}

function merge<T extends Person, U extends object>(objA: T, objB: U): T & U {
  return { ...objB, ...objA }
}

const mergedObject = merge(
  { name: 'Korn', age: 18 },
  { hobbies: ['Play game', 'Read book'] }
)
console.log(mergedObject.name, mergedObject.age)

interface Lengthy {
  length: number
}

function countAndDescribe<T extends Lengthy>(element: T): [T, string] {
  let descriptionText = 'Got no value.'
  if (element.length > 0) {
    descriptionText = `Got ${element.length} elements`
  }
  return [element, descriptionText]
}

console.log(countAndDescribe('Hi, there!'))
console.log(countAndDescribe(['Cat', 'Dog', 'Bird']))
console.log(countAndDescribe({ length: 2 }))

function extractAndConvert<T extends object, U extends keyof T>(
  obj: T,
  key: U
) {
  return `Value: ${obj[key]}`
}

console.log(extractAndConvert({ name: 'Korn', job: 'Empty' }, 'name'))
console.log(extractAndConvert({ name: 'Korn', job: 'Empty' }, 'job'))
console.log(extractAndConvert(['Korn', '18'], 1))

/* ================================================ */

class DataStorage<T> {
  private list: T[] = []

  addItem(item: T): void {
    this.list.push(item)
  }

  removeItem(item: T): void {
    this.list.splice(this.list.indexOf(item), 1)
  }

  getList(): Array<T> {
    return this.list
  }
}

const personStorage = new DataStorage<Person>()
personStorage.addItem({ name: 'Korn', age: 18 })
personStorage.addItem({ name: 'Sila', age: 20 })
personStorage.removeItem({ name: 'Sila', age: 20 })
personStorage.addItem({ name: 'fResult', age: 980 })
console.log(personStorage.getList())

interface CourseGoal {
  title: string
  description: string
  completeUntil: Date
}

function createCourseGoal(
  title: string,
  description: string,
  completeUntil: Date
): CourseGoal {
  // It can be assigned empty array ({}), because we defined type is a Partial of CourseGoal
  const courseGoal: Partial<CourseGoal> = {}
  courseGoal.title = title
  courseGoal.completeUntil = completeUntil
  courseGoal.description = description
  return courseGoal as CourseGoal
}

console.log(createCourseGoal('Typescript', 'Learn my Typescript', new Date()))

const hobbies: Readonly<string[]> = ['Read book', 'Surf the internet']
// hobbies[0] = 'a' // It cannot assign at index 0 because this array is readonly
// hobbies.push('b') // It cannot push anything because this array is readonly
