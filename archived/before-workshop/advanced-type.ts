type Admin = {
  name: string
  privileges: string[]
}

type Employee = {
  name: string
  startedDate: Date
}

// interface ElevatedEmployee extends Admin, Employee {}
type ElevatedEmployee = Admin & Employee

const employee1: ElevatedEmployee = {
  name: 'Korn',
  privileges: ['create-server'],
  startedDate: new Date('05/13/1991')
}

console.log(employee1)

// UNION TYPE
type Combinable = string | number
type Numeric = boolean | number

// INTERSECTION TYPE
type Universal = Combinable & Numeric

// FUNCTION OVERLOADING
function add(n1: number, n2: number): number
function add(n1: string, n2: string): string
function add(n1: string, n2: number): string
function add(n1: number, n2: string): string
function add(n1: Combinable, n2: Combinable): Combinable {
  if (typeof n1 === 'string' || typeof n2 === 'string') {
    return n1.toString() + n2.toString()
  }
  return n1 + n2
}

console.log(add(4, '5'))
const result = <string>add('Sila', ' Setthakan-anan')
console.log(result.split(' '))

type UnknownEmployee = Admin | Employee

function printEmployeeInformation(emp: UnknownEmployee) {
  console.log('Name:', emp.name)
  if ('privileges' in emp) {
    console.log('Privileges:', emp.privileges)
  }
  if ('startedDate' in emp) {
    console.log('Started Date:', emp.startedDate.toDateString())
  }
}

printEmployeeInformation(employee1)

class Car {
  drive() {
    console.log('Driving...')
  }
}

class Truck {
  constructor(public name?: string) {}

  drive() {
    console.log('Driving a truck...')
  }

  loadCargo(amount: number) {
    console.log('Loading cargo ...', amount)
  }
}

type Vehicle = Car | Truck

const vehicle1: Vehicle = new Car()
const vehicle2: Vehicle = new Truck()

function useVehicle(vehicle: Vehicle) {
  vehicle.drive()
  // if ('loadCargo' in vehicle) {
  if (vehicle instanceof Truck) {
    vehicle.loadCargo(1000)
  }
}

useVehicle(vehicle1)
useVehicle(vehicle2)

interface Bird {
  type: 'bird'
  flyingSpeed: number
}

interface Horse {
  type: 'horse'
  runningSpeed: number
}

// UNION TYPE
type Animal = Bird | Horse

function moveAnimal(animal: Animal) {
  let speed
  switch (animal.type) {
    case 'bird':
      speed = animal.flyingSpeed
      break
    case 'horse':
      speed = animal.runningSpeed
      break
    default:
      return
  }
  console.log('Moving speed:', `${animal.type} has speed ${speed}`)
}

moveAnimal({ flyingSpeed: 50, type: 'bird' })
moveAnimal({ type: 'horse', runningSpeed: 150 })

const greetingElem = <HTMLInputElement>document.querySelector('#greeting')
greetingElem.value = 'Hi, there.'

// { email: 'Not an invalid email', username: 'Must start with character' }
interface ErrorContainer {
  [prop: string]: string
}

const errorBag: ErrorContainer = {
  email: 'Not an invalid email',
  username: 'Must start with character'
}
console.log(errorBag.email, '/', errorBag.username)
