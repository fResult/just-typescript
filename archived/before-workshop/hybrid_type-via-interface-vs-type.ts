/*== Callable object (function) by type and interface ==*/
type TCounterFn = (start: number) => string

interface ICounterFn {
  (start: number): string
}

function counterFnType(start: number) {
  return start.toString()
}

function counterFnInterface(start: number) {
  return start.toString()
}

/*== Static object by type and interface ==*/
type CounterStaticType = {
  interval: number
  reset: () => void
}

interface CounterStaticInterface {
  interval: number
  reset(): void
}

/*== Hybrid ==*/
type HCounterType = TCounterFn & CounterStaticType
interface HCounterInterface extends ICounterFn, CounterStaticInterface {}

/*== Hybrid Types with both type alias and interface ==*/
interface CounterInterface {
  (start: number): string
  interval: number
  reset(): void
}

const getCounterHybridInterface = () => {
  const counter = ((start: number) => start.toString()) as CounterInterface
  counter.interval = 123
  counter.reset = () => {}
  return counter
}

const callableInterface = getCounterHybridInterface()
callableInterface(10)
callableInterface.reset()
callableInterface.interval = 5.0

type CounterType = {
  // callable part
  (start: number): string
  // static properties
  interval: number
  reset: () => void
}

const getCounterHybridType = () => {
  const counter = ((start: number) => start.toString()) as CounterType
  counter.interval = 123
  counter.reset = () => {}
  return counter
}

const callableType = getCounterHybridType()
callableType(25)
callableType.reset()
callableType.interval = 14
