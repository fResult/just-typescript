import { NextFunction, Request, RequestHandler, Response } from 'express'

import { Todo } from '../models/todo.model'

const TODOS: Array<Todo> = []

export const getTodos: RequestHandler = (req, res, next) => {
  res.json({ todos: TODOS })
}

export const createTodo: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  const newTodo = new Todo(Date.now().toString(), (<Todo>req.body).text)
  TODOS.push(newTodo)

  res.status(201).json({
    message: 'Created the Todo',
    createdTodo: newTodo,
    allTodos: TODOS
  })
}

export const updateTodo: RequestHandler<{ id: string }> = (req, res, next) => {
  const todoId = req.params.id
  const todoFromBody = req.body
  const foundTodo: Todo | undefined = TODOS.find((todo) => todo.id === todoId)
  if (!foundTodo) throw new Error(`Not found Todo`)
  const updatedTodo = Object.assign(foundTodo, todoFromBody)

  res.json({ message: `Updated todo ID: ${todoId}`, updatedTodo })
}

export const deleteTodo: RequestHandler<{ id: string }> = (req, res, next) => {
  const todoId = req.params.id

  const todoIdx = TODOS.findIndex((todo) => todo.id === todoId)
  if (todoIdx < 0) throw new Error(`Not found Todo`)
  TODOS.splice(todoIdx, 1)

  res.json({ message: `Delete todo ID: ${todoId}` })
}
