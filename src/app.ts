import express, { Express, NextFunction, Request, Response } from 'express'
import todoRoutes from './routes/todos.route'
import bodyParser from "body-parser";

const app: Express = express()
const PORT = 3333

app.use(bodyParser.json())
app.use('/todos', todoRoutes)

app.use((err: Error, req: Request, res: Response, next: NextFunction) => res.status(500).json({ message: err.message }))

app.listen(PORT, () => console.log(`Server is started on port ${PORT} nakub`))
